//
//  AuthController.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit
import WebKit

/**
Usage of SFSafariViewController will be a better solution, but it isn't available at iOS 9.0
 */
final
class AuthViewController: UIViewController {
    
    /**
     This is an inteded behaviour to crash app, if url doesnt exist at all
     */
    static var authURL: URL = {
        var components = URLComponents(string: "https://oauth.vk.com/authorize")!
        
        components.queryItems = [
            URLQueryItem(name: "client_id", value: Settings.shared[.vkClientID, default: ""]),
            URLQueryItem(name: "response_type", value: "token"),
            URLQueryItem(name: "v", value: "5.87"),
            URLQueryItem(name: "scope", value: "friends, wall"),
            URLQueryItem(name: "revoke", value: "1")
        ]
        
        return components.url!
    }()
    
    /**
     We should create this view programmatically, not in IB, because WKWebView has a bug with `initWithCoder:` prior iOS 11
     */
    weak var authView: WKWebView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if Settings.shared[.vkShouldReuseToken]  {
            Auth.shared.attempToAuthFromSecureStorage { (isSucceed) in
                if isSucceed {
                    self.openMainScene()
                    return
                }
            }
        }
        
        //Creation of webview
        let authView = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
        authView.translatesAutoresizingMaskIntoConstraints = false
        authView.navigationDelegate = self
        view.addSubview(authView)
        self.authView = authView
        
        authView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        authView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        authView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        authView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        let request = URLRequest(url: AuthViewController.authURL)
        authView.load(request)
    }
}

extension AuthViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(error)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

        if authView.url?.lastPathComponent == "blank.html" {
            var queryDict = [String: String]()
            authView.url!.absoluteString
                .split(separator: "#")
                .last!.split(separator: "&")
                .map{ $0.split(separator: "=") }
                .forEach{ queryDict[String($0[0])] = String($0[1]) }
            Auth.shared.resolve(userID: queryDict["user_id"]!, token: queryDict["access_token"]!)
            
            openMainScene()
        }
        
        activityIndicatorView.stopAnimating()
    }
    
    private func openMainScene() {
        //It can speed up our workflow, because we can start fetching data before actually go to new view controller
        PostController.shared.get(byIDs: []) {
            DispatchQueue.main.async {
                let mainController = (self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationViewController"))!
                self.present(mainController, animated: true, completion: nil)
            }
        }
    }
    
}
