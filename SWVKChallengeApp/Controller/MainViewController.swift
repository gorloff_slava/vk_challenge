//
//  ViewController.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

final
class MainViewController: UIViewController {
    
    @IBOutlet weak var avatarImageView: RoundedImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var postCountLabel: UILabel!
    
    // Activity views
    // Bottom one is intended to be used with bottom pull to refresh updats
    private let refreshControl = UIRefreshControl()
    @IBOutlet weak var bottomRefreshActivityView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Gradient
        let layer = CAGradientLayer()
        layer.frame = view.bounds
        layer.colors = [
            #colorLiteral(red: 0.9689999819, green: 0.976000011, blue: 0.9800000191, alpha: 0).cgColor,
            #colorLiteral(red: 0.9219999909, green: 0.92900002, blue: 0.9409999847, alpha: 1).cgColor
        ]
        view.layer.insertSublayer(layer, at: 0)
        
        //Avatar of the current user
        //We can add image of the user if it is presented yet
        avatarImageView.image = UserController.shared.currentUser?.image
        
        //Or we can get it later during async notification
        NotificationCenter.default.addObserver(forName: UserController.changeCurrentUserNotification, object: nil, queue: nil) { (_) in
            DispatchQueue.main.async {
                self.avatarImageView.image = UserController.shared.currentUser?.image
            }
        }
        
        //When we get new posts in store during async update we will receive notification and update components
        NotificationCenter.default.addObserver(forName: PostController.storeDidChangeNotification, object: nil, queue: nil) { (_) in
            self.store = PostController.shared.store
        }
        
        //New iOS can calculate row height dynamically but for old ones we will add some little help
        tableView.rowHeight = UITableView.automaticDimension
        if #available(iOS 12.0, *) {
            tableView.estimatedRowHeight = UITableView.automaticDimension
        } else {
            tableView.estimatedRowHeight = 535
        }
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(setNeedsUpdateStore), for: .valueChanged)
        
        // To ensure that the scroll is smooth we will add extra cells in reuse pool
        // Also we will divide expanded and normal cells
        for i in 0...4 {
            tableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "\(PostTableViewCell.expandedReuseIdentifier)_\(i)")
            tableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "\(PostTableViewCell.reuseIdentifier)_\(i)")
        }
    }
    
    // MARK: - Refreshing content
    
    /**
     Intended to be used with upper pull-to-refresh
     Remake the initial request (intended to add new posts)
     */
    @objc
    func setNeedsUpdateStore() {
        isInSearch = false
        isUpdateInProgress = true
        
        PostController.shared.get(byIDs: []) {
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.isUpdateInProgress = false
            }
        }
    }
    
    private var isUpdateInProgress = false
    
    /**
     Intended to be used for getting next information
     */
    func setNeedsUpdateNextStore() {
        bottomRefreshActivityView.startAnimating()
        postCountLabel.isHidden = true
        isUpdateInProgress = true
        
        isInSearch = false
        
        PostController.shared.getNext {
            DispatchQueue.main.async {
                self.postCountLabel.isHidden = false
                self.bottomRefreshActivityView.stopAnimating()
                self.isUpdateInProgress = false
            }
        }
    }
    
    // MARK: - Local store
    var store = PostController.shared.store {
        didSet {
            updatePostCount()
            
            DispatchQueue.main.async {
                self.expandedCellsNumbers = []
                self.tableView.reloadData()
            }
        }
    }
    
    private func updatePostCount() {
        let count = isInSearch ? searchedStore.count : store.count
        let suffix: String
        
        if count <= 20 {
            switch count {
            case 1:
                suffix = "запись"
            case 2...4:
                suffix = "записи"
            case 0, 5...20:
                suffix = "записей"
            default:
                suffix = ""
            }
        } else {
            switch count % 10 {
            case 1:
                suffix = "запись"
            case 2...4:
                suffix = "записи"
            case 0, 5...9:
                suffix = "записей"
            default:
                suffix = ""
            }
        }
        
        let countText = "\(count) \(suffix)"
        
        DispatchQueue.main.async {
            self.postCountLabel.text = countText
        }
    }
    
    // MARK: - Search functionality
    private var isInSearch = false {
        didSet {
            if !isInSearch {
                searchedStore = []
            }
            
            expandedCellsNumbers = []
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    var searchExpandedCellsNumbers = Set<IndexPath>()
    
    private var searchedStore = [(Post, [NSRange])]() {
        didSet {
            if isInSearch {
                updatePostCount()
                expandedCellsNumbers = []
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    /**
     Because search is an expensive operation we will do it in separate queue
     */
    private let searchQueue = DispatchQueue(label: "ru.swiftworld.searchqueue.\(UUID().uuidString)")
    
    var expandedCellsNumbers = Set<IndexPath>()
}

// MARK: - UITableViewDataSource
extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isInSearch ? searchedStore.count : store.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = expandedCellsNumbers.contains(indexPath) ? "\(PostTableViewCell.expandedReuseIdentifier)_\(indexPath.row % 5)" : "\(PostTableViewCell.reuseIdentifier)_\(indexPath.row % 5)"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! PostTableViewCell
        
        let post = isInSearch ? searchedStore[indexPath.row].0 : store[indexPath.row]
        cell.post = post
        cell.delegate = self
        
        //Higlighting the search results
        if isInSearch {
            let ranges = searchedStore[indexPath.row].1
            cell.highlightRanges(ranges)
        }
        
        //This section is needed for iOS 9, because it has problems with UITableView self-sizing cells
        if #available(iOS 12.0, *) {
            
        } else {
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
        }
        
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !(cell.reuseIdentifier?.contains(PostTableViewCell.expandedReuseIdentifier) ?? false) {
            let cell = cell as! PostTableViewCell
            DispatchQueue.main.async {
                cell.toggleExpandedText(false)
            }
        }
    }
    
}


// MARK: - PostTableViewCellDelegate
/**
 Because we need to hide the text of post and show expanded version we need to get AL-engine to recalculate table row height. Because we use stack view - we also get nice and smooth animation.
 */
extension MainViewController: PostTableViewCellDelegate {
    
    func postTableViewCellShowMoreButtonTapped(_ postTableViewCell: PostTableViewCell) {
        if postTableViewCell.isExpanded {
            if let indexPath = tableView.indexPath(for: postTableViewCell) {
                expandedCellsNumbers.insert(indexPath)
            }
        }
    }
    
    func postTableViewCellReqeustsRecalculating(_ postTableViewCell: PostTableViewCell) {
        if tableView.visibleCells.contains(postTableViewCell) {
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }

}

// MARK: - UISearchBarDelegate
extension MainViewController: UISearchBarDelegate {
    
    /**
     Because we should give the user fast experience we will start from search through exsisting local records.
     We will pay for it some computational cost, but get faster user experience
     */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.isEmpty else {
            return
        }
        
        searchBar.resignFirstResponder()
        isInSearch = true
        
        //Local presearch
        searchQueue.async {
            self.searchedStore = self.store.map { (post) -> (Post, [NSRange]) in
                return (post, post.text.matches(withSubstring: text))
            }.filter{ !$0.1.isEmpty }
        }
        
        searchQueue.async {
            SearchPostController.shared.get(byIDs: [], query: text) { (posts) in
                guard let posts = posts else {
                    return
                }
                
                let newResults = Set(posts).subtracting(Set(self.searchedStore.map{ $0.0 }))
                
                // Because regex search and image fetching are both expensive operations we can request image fetching and continue to search
                SearchPostController.shared.fetchImages(for: Array(newResults))
                
                let computedNewResults = newResults.map { (post) -> (Post, [NSRange]) in
                    return (post, post.text.matches(withSubstring: text))
                }.filter{ !$0.1.isEmpty }
                self.searchedStore = (computedNewResults + self.searchedStore).sorted { (leftPost, rightPost) -> Bool in
                     return leftPost.0.date > rightPost.0.date
                }
            }
        }
    
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        searchBar.text = ""
        isInSearch = false
    }
}

// MARK: - Bottom refresh control
extension MainViewController {
    
    #warning("Doesn't work now")
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if Int(scrollView.contentOffset.y + scrollView.frame.height) == Int(scrollView.contentSize.height + scrollView.contentInset.bottom) {
            
        }
    }
    
}
