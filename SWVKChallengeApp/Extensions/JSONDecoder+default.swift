//
//  JSONDecoder+default.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation

/**
 Small solution to small reduction of memory footprint
 */
extension JSONDecoder {
    
    static let `default` = JSONDecoder()
    
}
