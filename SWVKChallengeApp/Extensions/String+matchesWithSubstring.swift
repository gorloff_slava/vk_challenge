//
//  String+matchesWithSubstring.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 10/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Finds all matches of substring in given string and returns NSRange array.
 Intended to be used with attributed strings
 */
extension String {

    func matches(withSubstring substring: String) -> [NSRange]  {
        let regularExpressionString = String(format: "\\b%@", (substring as NSString))
        let regularExpression = try! NSRegularExpression(pattern: regularExpressionString, options: .caseInsensitive)
        
        /**
         We used this trick to fully incompatible option to represent base string as ICU compatible for regex search. And it works. Maybe.
         */
        let matches = regularExpression.matches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: NSString.CompareOptions.regularExpression.rawValue), range: NSRange(startIndex..., in: self))
        
        return matches.map{ $0.range }
    }
    
}
