//
//  UIDevice+modelName.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 11/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 This solution shouldn't be used in real apps due to is controversy with Apple advices
 But here we will use it for interface simplifying.
 */
extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else {
                return identifier
            }
            
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        return mapFromIdentifierToModelName(identifier: identifier)
    }()
    
    private static func mapFromIdentifierToModelName(identifier: String) -> String {
        switch identifier {
        case "iPhone5,1", "iPhone5,2":
            return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":
            return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":
            return "iPhone 5s"
        case "iPhone8,4":
            return "iPhone SE"
        case "iPhone4,1":
            return "iPhone 4s"
        case "iPod5,1":
            return "iPod Touch 5"
        case "iPod7,1":
            return "iPod Touch 6"
        case "i386", "x86_64":
            return "\(mapFromIdentifierToModelName(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
        default:
            return ""
        }
    }
    
    enum DeviceModel: String {
        case iPhone5 = "iPhone 5"
        case iPhone5c = "iPhone 5c"
        case iPhone5s = "iPhone 5s"
        case iPhoneSE = "iPhone SE"
        case iPhone4s = "iPhone 4s"
        case iPodTouch5 = "iPod Touch 5"
        case iPodTouch6 = "iPod Touch 6"
    }
    
    static var deviceModel: DeviceModel? {
        return DeviceModel(rawValue: modelName)
    }
    
    static let oldModels: [DeviceModel] = [
        .iPhone5,
        .iPhone5c,
        .iPhone5s,
        .iPhoneSE,
        .iPhone4s,
        .iPodTouch5,
        .iPodTouch6
    ]
    
    static var isOld: Bool {
        guard let deviceModel = deviceModel else {
            return false
        }
        
        return oldModels.contains(deviceModel)
    }
}
