//
//  UILabel+probableLineCount.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 10/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//
//

import UIKit

/**
 This approach is used in app to calculate probable number of lines in text.
 It doesnt work with multifont lines and its calculation is just approximate.
 But it can be used here as a fast solution
 */
extension UILabel {
    
    var probableLineCount: Int {
        let size = sizeThatFits(CGSize(width: frame.width, height: .greatestFiniteMagnitude))
        return max(Int(size.height / font.lineHeight), 0)
    }
    
}
