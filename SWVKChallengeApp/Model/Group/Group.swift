//
//  Group.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

final
class Group: Model, Decodable {
    
    let uid: String
    let name: String
    let photoURL: URL
    
    var image: UIImage?
    
    // MARK: - Decodable
    enum CodingKeys: String, CodingKey {
        case uid = "id"
        case name
        case photoURL = "photo_200"
        case additionalPhotoURL = "photo_100"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        uid = String(try values.decode(Int.self, forKey: .uid))
        name = try values.decode(String.self, forKey: .name)
        if let photoURL = try values.decodeIfPresent(URL.self, forKey: .photoURL) {
            self.photoURL = photoURL
        } else {
            self.photoURL = try values.decode(URL.self, forKey: .additionalPhotoURL)
        }
    }
    
}
