//
//  GroupController.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

final
class GroupController: ModelController {
    
    typealias BaseModel = Group
    
    static var shared = GroupController()
    private init() {
        self.store = []
    }
    
    var store: [Group] {
        didSet {
            fetchImages(for: store)
        }
    }
    
    func fetchImages(for models: [Group]) {
        for group in models where group.image == nil {
            ImageFetcher.shared.fetchImage(at: group.photoURL) { (data) in
                guard let data = data else {
                    return
                }
                
                SWSynchronized(group.image) {
                    group.image = UIImage(data: data)
                }
                
            }
        }
    }
}
