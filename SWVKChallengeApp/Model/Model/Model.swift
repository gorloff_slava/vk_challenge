//
//  Model.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

/**
 Basic protocol for models give them equatable and hashable for free using their id.
 */
protocol Model: Hashable {
    
    /**
     We used `uid` instead of `id` to be able to expose this part of code to Objc
     */
    var uid: String { get }
    
}

// MARK: - Equatable
extension Model {
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.uid == rhs.uid
    }
    
}

// MARK: - Hashable
extension Model {
    func hash(into hasher: inout Hasher) {
        hasher.combine(uid)
        hasher.combine("\(type(of: self))")
    }
}
