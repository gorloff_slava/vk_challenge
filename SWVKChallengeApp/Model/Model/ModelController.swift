//
//  ModelController.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation

/**
 The main purpose of this protocol is to provide some central store for models and its state.
 The disadvantage - we have to work with reference types
 But the plus - we can reduce memory footprint, because we will have just one copy of each object in memory at given time
 Also this store shoul maintain object graph, because we tried to make relations between things as week as possible
 */
protocol ModelController {
    
    associatedtype BaseModel: Model
    
//    static var shared: Self { get }
    func get(byIDs identifiers: [String], completionHandler: @escaping () -> Void)
    var store: [BaseModel] { get set }
    func fetchImages(for models: [BaseModel])
    
}

/**
 We need it to give behaviour similar to optional requirements - actually it doesnt work as optional requirement.
 But with this approach we can exclude dummy implementations of methods and other parts.
 The reason for usae - currently swift doesnt allow to create protocols with both assoc types and optional requirements (because the last part is a feature of objc runtime, while assoc types can only work in Swift.
 */
extension ModelController {
    
    func get(byIDs identifiers: [String], completionHandler: @escaping () -> Void) {
        fatalError("\(#function) not implemented yet")
    }
    
    func fetchImages(for models: [BaseModel]) {
        fatalError("\(#function) not implemented yet")
    }
}
