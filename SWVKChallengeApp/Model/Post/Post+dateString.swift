//
//  Post+dateString.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation

/**
 Ability to convert post date to preferrable string representation depending on year of current post
 */
extension Post {
    
    var dateString: String {
        let postYear = Calendar.current.dateComponents([.year], from: date)
        let currentYear = Calendar.current.dateComponents([.year], from: Date())
        if postYear == currentYear {
            return Post.currentYearFormatter.string(from: date)
        } else {
            return Post.otherYearsFormatter.string(from: date)
        }
    }
    
    private static let currentYearFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM в h:mm"
        return formatter
    }()
    
    private static let otherYearsFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        return formatter
    }()
    
}
