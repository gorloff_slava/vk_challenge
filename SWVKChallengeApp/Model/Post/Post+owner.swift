//
//  Post+owner.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 10/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation

/**
 Post owner declared weak because in stores this object always be presented in the same time.
 In other case the header of cell will just be collapsed
 */
extension Post {

    weak var owner: PostOwner? {
        if sourceID > 0 {
            return UserController.shared.store.first{ $0.uid == String(sourceID) }
        } else {
            return GroupController.shared.store.first{ $0.uid == String(-sourceID) }
        }
    }

}
