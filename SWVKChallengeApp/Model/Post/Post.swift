//
//  Post.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

final
class Post: Model, Decodable {
    
    let uid: String
    let date: Date
    let text: String
    var attachments: [UIImage]?
    var attachmentsURLs: [URL]
    
    let commentsCount: Int
    let likesCount: Int
    let repostsCount: Int
    let viewsCount: Int
    
    let sourceID: Int
    
    // MARK: - Decodable
    enum RootCodingKeys: String, CodingKey {
        case uid = "post_id"
        case additionalUID = "id"
        case date
        case text
        case attachments
        case comments
        case likes
        case reposts
        case views
        case sourceID = "source_id"
        case additionalSourceID = "owner_id"
    }
    
    enum CommentsCodingKeys: String, CodingKey {
        case count
    }
    
    enum LikesCodingKeys: String, CodingKey {
        case count
    }
    
    enum RepostsCodingKeys: String, CodingKey {
        case count
    }
    
    enum ViewsCodingKeys: String, CodingKey {
        case count
    }
    
    required init(from decoder: Decoder) throws {
        let rootValues = try decoder.container(keyedBy: RootCodingKeys.self)
        if let uid = try rootValues.decodeIfPresent(Int.self, forKey: .uid) {
            self.uid = String(uid)
        } else {
            self.uid = String(try rootValues.decode(Int.self, forKey: .additionalUID))
        }
       
        date = Date(timeIntervalSince1970: try rootValues.decode(Double.self, forKey: .date))
        text = try rootValues.decode(String.self, forKey: .text)
        if let sourceID = try rootValues.decodeIfPresent(Int.self, forKey: .sourceID) {
            self.sourceID = sourceID
        } else {
            self.sourceID = try rootValues.decode(Int.self, forKey: .additionalSourceID)
        }
        
        let commentsValues = try rootValues.nestedContainer(keyedBy: CommentsCodingKeys.self, forKey: .comments)
        commentsCount = try commentsValues.decode(Int.self, forKey: .count)
        
        let likesValues = try rootValues.nestedContainer(keyedBy: LikesCodingKeys.self, forKey: .likes)
        likesCount = try likesValues.decode(Int.self, forKey: .count)
        
        let repostsValues = try rootValues.nestedContainer(keyedBy: RepostsCodingKeys.self, forKey: .reposts)
        repostsCount = try repostsValues.decode(Int.self, forKey: .count)
        
        let viewValues = try? rootValues.nestedContainer(keyedBy: ViewsCodingKeys.self, forKey: .views)
        let possibleViewCount = try viewValues?.decode(Int.self, forKey: .count)
        viewsCount =  possibleViewCount ?? 0

        let attachmentsValues = try rootValues.decodeIfPresent([Attachment].self, forKey: .attachments)
        attachmentsURLs = []
        if let attachmentsValues = attachmentsValues {
            for attachment in attachmentsValues {
                guard let photo = attachment.photo else {
                    continue
                }
                
                guard let size = photo.sizes.filter({ $0.type == "q" }).first else {
                    continue
                }
                
                attachmentsURLs.append(size.url)
            }
        }
    }
}

/**
 Just a semi-private hierarchy in order to decode this strange thing
 */
fileprivate struct Attachment: Decodable {
    let photo: Photo?
    
    fileprivate struct Photo: Decodable {
        let sizes: [Size]
        
        fileprivate struct Size: Decodable {
            let type: String
            let url: URL
        }
    }
}
