//
//  PostController.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

final
class PostController: ModelController {
    typealias BaseModel = Post
    
    static var shared = PostController()
    private init() {
        self.store = []
    }
    
    var store: [Post] {
        didSet {
            fetchImages(for: store)
            
            NotificationCenter.default.post(Notification(name: PostController.storeDidChangeNotification))
        }
    }
    
    var startFrom: String?
    
    /**
     - parameter identifiers - currently unused, because we fetch all newsfeed related to current user limited by 50
     */
    func get(byIDs identifiers: [String], completionHandler: @escaping () -> Void) {
        get() {
            completionHandler()
        }
    }
    
    func getNext(completionHandler: @escaping () -> Void) {
        get(shouldContinue: true, completionHandler: completionHandler)
    }
    
    private func get(shouldContinue: Bool = false, completionHandler: (() -> Void)? = nil) {
        Network.shared.request(apiRequest: .newsfeedGet(shouldContinue, startFrom)) { (data, response, _) in
            guard let data = data, let _ = response else {
                return
            }
            
            do {
                let objectizedData = try JSONDecoder.default.decode(PostResponse.self, from: data)
                
                SWSynchronized(self.store) {
                    self.store = objectizedData.posts.filter { (post) in
                        return !(post.text.isEmpty && post.attachmentsURLs.isEmpty)
                    } // Here we filter posts without any images or text. They are not our clients
                }
                
                SWSynchronized(UserController.shared.store) {
                    UserController.shared.store = objectizedData.users
                }
                
                SWSynchronized(GroupController.shared.store) {
                    GroupController.shared.store = objectizedData.groups
                }
                
                SWSynchronized(self.startFrom) {
                    self.startFrom = objectizedData.startFrom
                }
                
            } catch {
                print(error)
            }
        }
        
        completionHandler?()
    }
    
    func fetchImages(for models: [Post]) {
        for post in models where post.attachments == nil || post.attachments!.isEmpty {
            post.attachments = []
            for url in post.attachmentsURLs {
                ImageFetcher.shared.fetchImage(at: url) { (data) in
                    guard let data = data, let image = UIImage(data: data) else {
                        return
                    }
                    
                    SWSynchronized(post.attachments) {
                        post.attachments!.append(image)
                    }
                }
            }
        }
    }
    
}

// MARK: - Notifications
extension PostController {
    
    static let storeDidChangeNotification = Notification.Name("PostController.storeDidChange")
    
}
