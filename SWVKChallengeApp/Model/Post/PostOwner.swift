//
//  PostOwner.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 10/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Should be used to interchangably add both user and group into news
 */
protocol PostOwner: class {
    
    var title: String { get }
    var image: UIImage? { get }
    
}

extension User: PostOwner {
    
    var title: String {
        return name!
    }
    
}

extension Group: PostOwner {
    
    var title: String {
        return name
    }
    
}
