//
//  PostResponse.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation

/**
 The main purpose of this type - to lower the degree of difficulty to extract this API to objects
 */
struct PostResponse: Decodable {
    
    let posts: [Post]
    let users: [User]
    let groups: [Group]
    var startFrom: String?
    
    // MARK: - Decodable
    enum RootCodingKeys: String, CodingKey {
        case response
    }
    
    enum SubCodingKeys: String, CodingKey {
        case items
        case profiles
        case groups
        case startFrom = "next_from"
    }
    
    init(from decoder: Decoder) throws {
        let root = try decoder.container(keyedBy: RootCodingKeys.self)
        let values = try root.nestedContainer(keyedBy: SubCodingKeys.self, forKey: .response)
        
        users = try values.decode([User].self, forKey: .profiles)
        groups = try values.decode([Group].self, forKey: .groups)
        posts = try values.decode([Post].self, forKey: .items)
        
        startFrom = try values.decodeIfPresent(String.self, forKey: .startFrom)
    }
}
