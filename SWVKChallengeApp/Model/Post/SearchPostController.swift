//
//  SearchPostController.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 11/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit


/**
 Please not that this class is not an ModelController
 This store intended to be used with temporal storage for search results.
 */
final
class SearchPostController {
    
    typealias BaseModel = Post
    
    static let storeDidChangeNotification = Notification.Name("SearchPostController.storeDidChange")
    
    static let shared = SearchPostController()
    private init() {
        store = []
    }
    
    var store: [Post] {
        didSet {
            fetchImages(for: store)
            
            NotificationCenter.default.post(Notification(name: SearchPostController.storeDidChangeNotification))
        }
    }
    
    var startFrom: String?
    
    // MARK: - Get posts
    func get(byIDs identifiers: [String], query: String, completionHandler: @escaping ([Post]?) -> Void) {
        get(query: query, completionHandler: completionHandler)
    }
    
    func getNext(query: String, completionHandler: @escaping ([Post]?) -> Void) {
        get(shouldContinue: true, query: query, completionHandler: completionHandler)
    }
    
    private func get(shouldContinue: Bool = false, query: String, completionHandler: (([Post]?) -> Void)? = nil) {
        Network.shared.request(apiRequest: .newsfeedSearch(shouldContinue, startFrom, query)) { (data, response, _) in
            guard let data = data, let _ = response else {
                completionHandler?(nil)
                return
            }
            
            do {
                let objectizedData = try JSONDecoder.default.decode(PostResponse.self, from: data)

                SWSynchronized(self.store) {
                    self.store = objectizedData.posts.filter { (post) in
                        return !(post.text.isEmpty && post.attachmentsURLs.isEmpty)
                    }
                }
                
                SWSynchronized(UserController.shared.store) {
                    UserController.shared.store = Array(Set(UserController.shared.store + objectizedData.users))
                }
                
                SWSynchronized(GroupController.shared.store) {
                    GroupController.shared.store = Array(Set(GroupController.shared.store + objectizedData.groups))
                }
                
                SWSynchronized(self.startFrom) {
                    self.startFrom = objectizedData.startFrom
                }
                
                completionHandler?(self.store)
            } catch {
                completionHandler?(nil)
                print(error)
            }
        }
    }
    
    // MARK: - Fetch Images
    func fetchImages(for models: [Post]) {
        for post in models where post.attachments == nil || post.attachments!.isEmpty {
            post.attachments = []
            for url in post.attachmentsURLs {
                ImageFetcher.shared.fetchImage(at: url) { (data) in
                    guard let data = data, let image = UIImage(data: data) else {
                        return
                    }
                    
                    SWSynchronized(post.attachments) {
                        post.attachments!.append(image)
                    }
                }
            }
        }
    }
    
}
