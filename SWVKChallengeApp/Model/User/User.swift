//
//  User.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

final
class User: Model, Decodable {
    
    let uid: String
    var name: String?
    
    private(set) var photoURL: URL?
    var image: UIImage?
    
    init(id: String) {
        self.uid = id
    }
    
    // MARK: - Decodable implementation
    enum CodingKeys: String, CodingKey {
        case uid = "id"
        case firstName = "first_name"
        case lastName = "last_name"
        case photoURLString = "photo_200"
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        uid = String(try values.decode(Int.self, forKey: .uid))
        
        let firstName = try values.decode(String.self, forKey: .firstName)
        let lastName = try values.decode(String.self, forKey: .lastName)
        name = "\(firstName) \(lastName)"
        
        photoURL = try values.decodeIfPresent(URL.self, forKey: .photoURLString)
    }
}

