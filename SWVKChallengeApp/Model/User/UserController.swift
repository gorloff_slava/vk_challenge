//
//  UserController.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

final
class UserController: ModelController {
    typealias BaseModel = User
    
    static let shared = UserController()
    private init() {
        self.store = []
    }
    
    var store: [User] {
        didSet {
            fetchImages(for: store)
        }
    }
    
    var currentUser: User? {
        didSet {
            guard let currentUser = currentUser else {
                return
            }
            
            fetchImages(for: [currentUser])
        }
    }
    
    func fetchImages(for models: [User]) {
        for user in models where user.image == nil {
            guard let photoURL = user.photoURL else {
                continue
            }
            
            ImageFetcher.shared.fetchImage(at: photoURL) { (data) in
                guard let data = data else {
                    return
                }
                
                SWSynchronized(user.image) {
                    user.image = UIImage(data: data)
                }
                
                if user == self.currentUser {
                    NotificationCenter.default.post(Notification(name: UserController.changeCurrentUserNotification))
                }
            }
        }
    }
}

// MARK: - Notifications
extension UserController {
    
    static let changeCurrentUserNotification = Notification.Name("changeCurrentUserNotification")
    
}
