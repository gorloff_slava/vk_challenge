//
//  Auth.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Singleton for all auth processes
 */
final
class Auth {
    
    static let shared = Auth()
    private init() {
        
    }
    
    private(set) var token: String!
    
    func resolve(userID: String, token: String) {
        self.token = token
        UserController.shared.currentUser = User(id: userID)
        
        Network.shared.request(apiRequest: .getCurrentUser) { (data, response, _) in
            guard let data = data, let _ = response else {
                return
            }
            
            do {
                let objectizedData = try JSONDecoder.default.decode([String: [User]].self, from: data)
                guard let user = objectizedData["response"]?.first else {
                    print("No user gotten via Auth")
                    return
                }
                
                UserController.shared.currentUser = user
            } catch {
                print(error)
            }
        }
        
        if Settings.shared[.vkShouldReuseToken] {
            SecureStorage.shared["token"] = token
            SecureStorage.shared["userID"] = userID
        }
    }
    
    /**
     Tries to login using token and userID from Keychain
     */
    func attempToAuthFromSecureStorage(completionHandler: @escaping (Bool) -> Void) {
        guard
            let token = SecureStorage.shared["token"],
            !token.isEmpty,
            let userID = SecureStorage.shared["userID"],
            !userID.isEmpty else {
                completionHandler(false)
                return
        }
        
        resolve(userID: userID, token: token)
        completionHandler(true)
    }
    
    func logout(completionHandler: @escaping () -> Void) {
        token = nil
        UserController.shared.currentUser = nil
        if Settings.shared[.vkShouldReuseToken] {
            SecureStorage.shared["token"] = nil
            SecureStorage.shared["userID"] = nil
        }
        
        UserController.shared.store = []
        PostController.shared.store = []
        GroupController.shared.store = []
        
        completionHandler()
    }
}
