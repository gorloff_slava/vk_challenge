//
//  APIError.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 10/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Provides distinct type for api error, because resulted object from API by default is not a Network error.
 */
struct APIError: Error, Decodable {

    let code: Int
    let message: String
    
    enum RootCodingKeys: String, CodingKey {
        case error
    }
    
    enum SubCodingKeys: String, CodingKey {
        case code = "error_code"
        case message = "error_msg"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: RootCodingKeys.self)
        let errorValues = try values.nestedContainer(keyedBy: SubCodingKeys.self, forKey: .error)
        code = try errorValues.decode(Int.self, forKey: .code)
        message = try errorValues.decode(String.self, forKey: .message)
    }
}
