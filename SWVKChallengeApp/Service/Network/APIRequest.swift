//
//  APIRequest.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation

/**
 Incapsulates different types of requests to VK API used in this App
 */
enum APIRequest {
    
    case usersGet([String])
    case getCurrentUser
    case newsfeedGet(Bool, String?)
    case newsfeedSearch(Bool, String?, String)
    
    /**
     Because this can be only used with well defined apis, we should force crash in case of problems
     */
    func url(baseURL: URL) -> URL {
        var components = URLComponents(url: baseURL, resolvingAgainstBaseURL: true)!
        components.queryItems = []
        
        switch self {
        case .usersGet(let ids):
            components.queryItems = [
                URLQueryItem(name: "user_ids", value: ids.joined(separator: ",")),
                URLQueryItem(name: "fields", value: "photo_200")
            ]
            
        case .getCurrentUser:
            components.path = "/method/users.get"
            components.queryItems = [
                URLQueryItem(name: "user_ids", value: UserController.shared.currentUser!.uid),
                URLQueryItem(name: "fields", value: "photo_200")
            ]
            
        case .newsfeedGet(let shouldContinue, let startFrom):
            components.path = "/method/newsfeed.get"
            components.queryItems = [
                URLQueryItem(name: "filters", value: "post"),
                URLQueryItem(name: "fields", value: "photo_200,photo_100")
            ]
            
            if shouldContinue, let startFrom = startFrom {
                components.queryItems?.append(URLQueryItem(name: "start_from", value: startFrom))
            }

        case .newsfeedSearch(let shouldContinue, let startFrom, let query):
            components.path = "/method/newsfeed.search"
            components.queryItems = [
                URLQueryItem(name: "filters", value: "post"),
                URLQueryItem(name: "fields", value: "photo_200,photo_100"),
                URLQueryItem(name: "q", value: query),
                URLQueryItem(name: "extended", value: "1")
            ]
            
            if shouldContinue, let startFrom = startFrom {
                components.queryItems?.append(URLQueryItem(name: "start_from", value: startFrom))
            }
        }
        
        components.queryItems!.append(URLQueryItem(name: "access_token", value: Auth.shared.token))
        components.queryItems!.append(URLQueryItem(name: "v", value: "5.87"))
        
        return components.url!
    }
    
    func request(baseURL: URL) -> URLRequest {
        var request = URLRequest(url: url(baseURL: baseURL))
        
        switch self {
        case .usersGet, .getCurrentUser, .newsfeedGet, .newsfeedSearch:
            request.httpMethod = HTTPMethod.GET.rawValue
        }
        
        return request
    }
    
}
