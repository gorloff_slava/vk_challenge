//
//  ImageFetcher.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation

/**
 This class uses distinct queue to get images from internet sources
 Also it provides memory (512MB) and on-disk caches for resulted images (1024MB)
 */
final
class ImageFetcher {
    
    static let shared = ImageFetcher()
    private init() {
        
    }
    
    private let sessionQueue = OperationQueue()
    private lazy var session: URLSession = {
        let cache = URLCache(
            memoryCapacity: 512 * 1024 * 1024,
            diskCapacity: 1024 * 1024 * 1024,
            diskPath: "images"
        )
        
        var configuration = URLSessionConfiguration.default
        configuration.urlCache = cache
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        
        return URLSession(configuration: configuration, delegate: nil, delegateQueue: sessionQueue)
    }()
    
    func fetchImage(at url: URL, completionHandler: @escaping (Data?) -> Void) {
        session.dataTask(with: url) { (data, response, error) in
            guard error == nil, let data = data, let _ = response else {
                print(error!)
                completionHandler(nil)
                return
            }
            
            completionHandler(data)
        }.resume()
    }
    
}
