//
//  Network.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation
import UIKit

/**
 Intended to be used as a point to VK API
 All requests cannot be cached
 */
final
class Network {
    
    static let shared = Network()
    private init() {
        
    }
    
    private let baseURL = URL(string: "https://api.vk.com/")!
    private let networkQueue = OperationQueue()
    private lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: networkQueue)
        return session
    }()
    
    func request(apiRequest: APIRequest, completionHandler: @escaping (Data?, HTTPURLResponse?, APIError?) -> Void ) {
        let request = apiRequest.request(baseURL: baseURL)
        
        session.dataTask(with: request) { (data, response, error) in
            guard error == nil, let data = data, let response = response else {
                print(error!)
                completionHandler(nil, nil, nil)
                return
            }
            
            if let apiError = try? JSONDecoder.default.decode(APIError.self, from: data) {
                print(apiError)
                
                guard apiError.code != 5 else {
                    Auth.shared.logout {
                        /**
                         If token is expired this block of code open the root view controller.
                         Now it produces the warning
                         ```
                         Warning: Attempt to dismiss from view controller <SWVKChallengeApp.AuthViewController: 0x7ffc0b02e700> while a presentation or dismiss is in progress!
                         ```
                         Because it can be called via initial presentation
                         */
                    
                        DispatchQueue.main.async {
                            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
                        }
                    }
                    return
                }
                
                completionHandler(nil, nil, apiError)
                return
            }

            completionHandler(data, response as? HTTPURLResponse, nil)
        }.resume()
    }
}
