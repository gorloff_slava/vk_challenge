//
//  SecureStorage.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 10/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation
import Security

/**
 Basic swift wrapper upon Keychain
 */
final
class SecureStorage {
    
    static let shared = SecureStorage()
    private init() {
        
    }
    
    subscript(key: String) -> String? {
        get {
            return get(forKey: key)
        }
        set {
            DispatchQueue.global().sync(flags: .barrier) {
                self.set(newValue, forKey: key)
            }
        }
    }
    
    private func makeQuery(key: String) -> NSMutableDictionary {
        let result = NSMutableDictionary()
        
        result[kSecClass as String] = kSecClassGenericPassword
        result[kSecAttrService as String] = key
        result[kSecAttrAccessible as String] = kSecAttrAccessibleAlwaysThisDeviceOnly
        
        return result
    }
    
    private func set(_ string: String?, forKey key: String) {
        let query = makeQuery(key: key)
        guard let data = string?.data(using: .utf8, allowLossyConversion: false) else {
            SecItemDelete(query)
            return
        }
        
        if SecItemCopyMatching(query, nil) == noErr {
            SecItemUpdate(query, [kSecValueData: data] as NSDictionary)
        } else {
            query[kSecValueData as String] = data
            SecItemAdd(query, nil)
        }
    }
    
    private func get(forKey key: String) -> String? {
        let query = makeQuery(key: key)
        query[kSecReturnData as String] = kCFBooleanTrue
        query[kSecReturnAttributes as String] = kCFBooleanTrue
        
        var result: CFTypeRef?
        let status = SecItemCopyMatching(query, &result)
        
        guard
            let dictResult = result as? NSDictionary,
            let data = dictResult[kSecValueData as String] as? Data,
            status == noErr else {
                return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
}
