//
//  Settings.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 10/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Provides global immutable settings storage from `ENV` file.
 */
final
class Settings {

    static let shared = Settings()
    private init() {
        let settingsUrl = Bundle.main.url(forResource: "ENV", withExtension: "plist")!
        backStore = NSDictionary(contentsOf: settingsUrl)!
    }
    
    private var backStore: NSDictionary
    
    enum Key {
        case vkClientID
        case vkShouldReuseToken
    }
    
    subscript(key: Key, default defaultValue: String) -> String {
        switch key {
        case .vkClientID:
            return ((backStore["VK"] as! NSDictionary)["client_id"] as! String?) ?? defaultValue
        default:
            return ""
        }
    }
    
    subscript(key: Key) -> Bool {
        switch key {
        case .vkShouldReuseToken:
            return ((backStore["VK"] as! NSDictionary)["should_reuse_token"] as! Int) == 1
        default:
            return false
        }
    }
    
}
