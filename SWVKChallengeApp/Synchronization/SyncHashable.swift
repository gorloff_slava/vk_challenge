//
//  SyncHashable.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 11/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation

/**
 Because we can build up synced state for different types - this protocol provides ability to distinguish objects between types and use them in one place
 */
protocol SyncHashable: Hashable {
    
}

extension SyncHashable {
    
    var syncedHash: String {
        return "\(type(of: self))_\(hashValue)"
    }
    
}

/**
 Little trick - we give all arrays conformance to our protocol
 */
extension Array: SyncHashable where Element: Hashable {}
extension Set: SyncHashable where Element: Hashable {}
extension Dictionary: SyncHashable where Value: Hashable {}
extension Optional: SyncHashable where Wrapped: Hashable {}
extension Bool: SyncHashable {}
