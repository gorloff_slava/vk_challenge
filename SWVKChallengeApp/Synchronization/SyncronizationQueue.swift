//
//  SyncronizationQueue.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 10/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import Foundation
import Dispatch

/**
 Classical bareer queue which provides ability to multi-thread execution blocks of code locking some object (or valuetype, too)
 */
final
class SyncronizationQueue {
    
    private let isolationQueue = DispatchQueue(label: "ru.swiftworld.isolation_queue.\(UUID().uuidString)", attributes: [.concurrent])
    
    func async(block: @escaping () -> Void) {
        isolationQueue.async(flags: .barrier) {
            block()
        }
    }
    
    func sync(block: () -> Void) {
        isolationQueue.sync(flags: .barrier) {
            block()
        }
    }
    
}

fileprivate
extension SyncronizationQueue {
    
    /**
     Please use distinct key for each object to distinguish queue
     This storage lucks ability to purging unused queues
    */
    static var queueStore: [String: SyncronizationQueue] = [:]
    
    static let selfSyncQueue = SyncronizationQueue()
    
}

/**
 If two objects produce the same hash - they will share the same queue and degrade speed
 But this solution here is simple for that program - we don't need to distinguish all objects and produce additional mechanisms
 */
func SWSynchronized<T: SyncHashable>(_ instance: T?, block: () -> Void) {
    var queue: SyncronizationQueue!
    
    // We need syncing only for writing consistency. So we can slightly speed up sync ops checking queue for existance
    // But in fail option we should recheck existance self-synced to exclude false positive
    if let existedQueue = SyncronizationQueue.queueStore[instance.syncedHash] {
        queue = existedQueue
    } else {
        SyncronizationQueue.selfSyncQueue.sync {
            if let existedQueue = SyncronizationQueue.queueStore[instance.syncedHash] {
                queue = existedQueue
            } else {
                queue = SyncronizationQueue()
                SyncronizationQueue.queueStore[instance.syncedHash] = queue
            }
        }
    }
    
    queue.sync {
        block()
    }
    
}
