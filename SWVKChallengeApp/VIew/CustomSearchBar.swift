//
//  CustomSearchBar.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

final
class CustomSearchBar: UISearchBar {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        //To remove background
        backgroundImage = UIImage()
        
        //TextField is normally inaccessible
        let textField = value(forKey: "searchField") as? UITextField
        textField?.backgroundColor = #colorLiteral(red: 0, green: 0.1099999994, blue: 0.2399999946, alpha: 0.05999999866)
        textField?.tintColor = #colorLiteral(red: 0.3333333333, green: 0.5529411765, blue: 0.7921568627, alpha: 1)
        
        tintColor = #colorLiteral(red: 0.3333333333, green: 0.5529411765, blue: 0.7921568627, alpha: 1)
    }

}
