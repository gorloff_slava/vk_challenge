//
//  PostTableViewCell+highlightRanges.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 11/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Provied ability to Cell to highlight ranges in post string
 */
extension PostTableViewCell {
    
    func highlightRanges(_ ranges: [NSRange]) {
        
        //We need to create copy, because if we use the old instance the UILabel will refuse changes due to its internal mechanics
        guard let attributedText = internalTextLabel.attributedText?.mutableCopy() as? NSMutableAttributedString else {
            return
        }
        
        //It is needed to highlight later all parts
        attributedText.addAttribute(.backgroundColor, value: UIColor.clear, range: NSRange(location: 0, length: attributedText.length))
        for range in ranges {
//            #colorLiteral(red: 1, green: 0.9568627451, blue: 0.8823529412, alpha: 1)
            attributedText.addAttribute(.backgroundColor, value: #colorLiteral(red: 1, green: 0.9568627451, blue: 0.8823529412, alpha: 1), range: range)
            attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.7450980392, green: 0.5294117647, blue: 0.1647058824, alpha: 1), range: range)
        }
        internalTextLabel.attributedText = attributedText
        expanedTextLabel.attributedText = attributedText
    }
    
    func clearHighlithing() {
        guard let attributedText = internalTextLabel.attributedText as? NSMutableAttributedString else {
            return
        }
        
        attributedText.removeAttribute(.backgroundColor, range: NSRange(location: 0, length: attributedText.length))
    }
    
}
