//
//  PostTableViewCell.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Cell for Post
 */
final
class PostTableViewCell: UITableViewCell {
    
    static let reuseIdentifier = "PostTableViewCell"
    static let expandedReuseIdentifier = "ExpandedPostTableViewCell"

    @IBOutlet weak var avatarImageView: RoundedImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    // We use 2 different labels - one for normal state; and another for expanded
    @IBOutlet weak var internalTextLabel: UILabel!
    @IBOutlet weak var expanedTextLabel: UILabel!
    
    @IBOutlet weak var postImageView: UIImageView!
    private var postImageViewAspectConstraint: NSLayoutConstraint? {
        didSet(oldConstraint) {
            if let oldConstraint = oldConstraint {
                postImageView.removeConstraint(oldConstraint)
            }
            if let postImageViewAspectConstraint = postImageViewAspectConstraint {
                postImageView.addConstraint(postImageViewAspectConstraint)
            }
        }
    }
    
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!
    @IBOutlet weak var repostsCountLabel: UILabel!
    @IBOutlet weak var viewCountLabel: UILabel!
    
    @IBOutlet weak var statMenuStackView: UIStackView!
    @IBOutlet weak var statSubMenuStackView: UIStackView!
    @IBOutlet var statImagesConstraintCollection: [NSLayoutConstraint]!
    @IBOutlet weak var viewCountImageWidthConstraint: NSLayoutConstraint!
    
    weak var post: Post? {
        didSet {
            guard let post = post else {
                return
            }
            
            //Date
            dateLabel.text = post.dateString
            
            //Attributed Text
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.minimumLineHeight = 22
            paragraphStyle.maximumLineHeight = 22
            paragraphStyle.lineBreakMode = .byTruncatingTail
            
            let attributedText = NSMutableAttributedString(string: post.text, attributes: [
                .paragraphStyle: paragraphStyle,
                .kern: -0.17
            ])
            
            //Detecting links - alternative solution was using TextView, but it add additional problems with text maintaining
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let basicString = attributedText.string
            detector.enumerateMatches(in: basicString, options: [], range: NSRange(location: 0, length: basicString.utf16.count)) { (result, _, _) in
                guard let url = result?.url, let range = result?.range else {
                    return
                }
                
                attributedText.addAttributes([
                    .foregroundColor: #colorLiteral(red: 0.3333333333, green: 0.5529411765, blue: 0.7921568627, alpha: 1)
                ], range: range)
            }
            
            internalTextLabel.attributedText = attributedText
            expanedTextLabel.attributedText = attributedText
            if internalTextLabel.probableLineCount <= 8 {
                showMoreButton.isHidden = true
                showMoreButtonHeightConstraint.constant = 0
                setNeedsUpdateConstraints()
            }
            
            //Image
            if let images = post.attachments, !images.isEmpty {
                
                //One Image
                if images.count == 1 {
                    let image = images.first!
                    let aspectRatio = image.size.width / image.size.height
                    let constraint = postImageView.widthAnchor.constraint(equalTo: postImageView.heightAnchor, multiplier: aspectRatio)
                    constraint.priority = UILayoutPriority(rawValue: 999)
                    postImageViewAspectConstraint = constraint
                    
                    postImageView.image = image
                    toggleMultipleAnnotations(false)
                } else if images.count > 1 { //Multiple Images
                    postImageView.image = nil
                    toggleMultipleAnnotations(true)
                    carouselImageView.reloadData()
                }
            } else {
                postImageView.image = nil
                toggleMultipleAnnotations(false)
            }
            
            //Likes and other stats
            func format(count: Int) -> String {
                let ungranularedCount = (Double(count) / 1000).rounded()
                if ungranularedCount > 0 {
                    return "\(Int(ungranularedCount))K"
                } else {
                    return "\(count)"
                }
            }

            viewCountLabel.text = format(count: post.viewsCount)
            likesCountLabel.text = format(count: post.likesCount)
            repostsCountLabel.text = format(count: post.repostsCount)
            commentsCountLabel.text = format(count: post.commentsCount)
            
            //Owner
            avatarImageView.image = post.owner?.image
            nameLabel.text = post.owner?.title
            
            if isExpanded {
                toggleExpandedText(true)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        postImageViewAspectConstraint = nil
        carouselImageView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        //Shadows
        layer.shadowColor = #colorLiteral(red: 0.39, green: 0.4, blue: 0.44, alpha: 0.07).cgColor
        layer.masksToBounds = false
        layer.shadowOpacity = 1
        layer.shadowRadius = 18
        layer.shadowOffset = CGSize(width: 0, height: 24)

        configureCarouselImageView()
        
        // Very bad hack for old devices
        if UIDevice.isOld {
            let desiredFontSize: CGFloat = 12
            for label in [viewCountLabel!, likesCountLabel!, repostsCountLabel!, commentsCountLabel!] {
                label.font = label.font.withSize(desiredFontSize)
            }
            
            let scaleFactor = desiredFontSize / 14
            for constraint in statImagesConstraintCollection {
                constraint.constant *= scaleFactor
            }
            
            viewCountImageWidthConstraint.constant *= scaleFactor
            statMenuStackView.spacing *= scaleFactor
            statSubMenuStackView.spacing *= scaleFactor
            statSubMenuStackView.spacing *= 0.75
        }
    }
    
    lazy var isExpanded = {
       return reuseIdentifier?.contains(PostTableViewCell.expandedReuseIdentifier) ?? false
    }()
    @IBOutlet weak var showMoreButton: UIButton!
    @IBOutlet weak var showMoreButtonHeightConstraint: NSLayoutConstraint!
    @IBAction func showMoreButtonTapped() {
        toggleExpandedText(true)
        delegate?.postTableViewCellShowMoreButtonTapped(self)
    }
    
    func toggleExpandedText(_ shouldToggle: Bool) {
        isExpanded = shouldToggle
        internalTextLabel.isHidden = isExpanded
        expanedTextLabel.isHidden = !isExpanded
        showMoreButton.isHidden = isExpanded
        showMoreButtonHeightConstraint.constant = shouldToggle ? 0 : 30
        setNeedsUpdateConstraints()
        if isExpanded {
            delegate?.postTableViewCellReqeustsRecalculating(self)
        }
    }
    
    weak var delegate: PostTableViewCellDelegate?
    
    // MARK: - Photo carousel implementation
    @IBOutlet weak var carouselImageView: UICollectionView!
    @IBOutlet var carouselImageViewHeightConstraint: NSLayoutConstraint!
    
    private func configureCarouselImageView() {
        carouselImageView.register(UINib(nibName: PostTableViewCellPhotoCellCollectionViewCell.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: PostTableViewCellPhotoCellCollectionViewCell.reuseIdentifier)
        carouselImageView.delegate = self
        carouselImageView.dataSource = self
    }
    
    private func toggleMultipleAnnotations(_ shouldToggle: Bool) {
        carouselImageView.isHidden = !shouldToggle
        carouselImageViewHeightConstraint.constant = shouldToggle ? 253 : 0
//        setNeedsUpdateConstraints()
        delegate?.postTableViewCellReqeustsRecalculating(self)
    }
}

protocol PostTableViewCellDelegate: class {
    func postTableViewCellShowMoreButtonTapped(_ postTableViewCell: PostTableViewCell)
    func postTableViewCellReqeustsRecalculating(_ postTableViewCell: PostTableViewCell)
}

// MARK: Collection View Protocols
extension PostTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = post?.attachments?.count else {
            return 0
        }
        
        return count > 1 ? count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PostTableViewCellPhotoCellCollectionViewCell.reuseIdentifier, for: indexPath) as! PostTableViewCellPhotoCellCollectionViewCell
        
        cell.photoImageView.image = post?.attachments?[indexPath.item]
        
        return cell
    }
    
    
}
extension PostTableViewCell: UICollectionViewDelegate {}
extension PostTableViewCell: UICollectionViewDelegateFlowLayout {}
