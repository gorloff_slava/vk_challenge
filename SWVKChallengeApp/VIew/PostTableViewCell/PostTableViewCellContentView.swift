//
//  PostTableViewCellContentView.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Small class for content view for tableViewCell with rounded corners
 */
final
class PostTableViewCellContentView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.masksToBounds = true
        layer.cornerRadius = 10
    }

}
