//
//  PostTableViewCellPhotoCellCollectionViewCell.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 11/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Simple cell for photo displaying
 */
final
class PostTableViewCellPhotoCellCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "PostTableViewCellPhotoCellCollectionViewCell"
    
    @IBOutlet weak var photoImageView: UIImageView!

}
