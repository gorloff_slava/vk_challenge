//
//  RoundedImageView.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 Intended to be used with 1:1 images
 */
final
class RoundedImageView: UIImageView {
    
    private func makeRounded() {
        if bounds.width == bounds.height {
            layer.cornerRadius = bounds.width / 2
            clipsToBounds = true
            layer.masksToBounds = true
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        makeRounded()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        makeRounded()
    }

    /**
     We added this method here, becuase with our layout the initial view isnt square
     */
    override func layoutSubviews() {
        super.layoutSubviews()
        
        makeRounded()
    }
}
