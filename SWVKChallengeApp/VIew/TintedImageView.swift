//
//  TintedImageView.swift
//  SWVKChallengeApp
//
//  Created by Вячеслав Горлов on 09/11/2018.
//  Copyright © 2018 Swift World. All rights reserved.
//

import UIKit

/**
 It is needed to add color to icons
 */
final
class TintedImageView: UIImageView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        tintColorDidChange()
    }

}
